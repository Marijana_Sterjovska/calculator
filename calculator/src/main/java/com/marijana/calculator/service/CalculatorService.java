package com.marijana.calculator.service;

import org.springframework.stereotype.Service;

@Service
public class CalculatorService {
	
	public Double add(Double num1, Double num2) {
		return num1+num2;
	}
	public Double substract(Double num1, Double num2) {
		return num1-num2;
	}
	public Double multiply(Double num1, Double num2) {
		return num1*num2;
	}
	public Double divide(Double num1, Double num2) {
		return num1/num2;
	}
	public Double pow(Double num1, Double num2) {
		return Math.pow(num1, num2);
	}
}
