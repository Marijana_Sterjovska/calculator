package com.marijana.calculator.controller;

import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.marijana.calculator.service.CalculatorService;

@RestController
@Validated
@RequestMapping("/calculator")
public class CalculatorController {

	@Autowired
	CalculatorService service;

	@GetMapping
	public Double calculate(@RequestParam(value = "operator") String operator,
			@RequestParam(value = "num1") @Min(1) double num1, @RequestParam(value = "num2") @Min(1) double num2) {

		switch (operator) {
		case "+":
			return service.add(num1, num2);
		case "-":
			return service.substract(num1, num2);
		case "*":
			return service.multiply(num1, num2);
		case "/":
			return service.divide(num1, num2);
		case "^":
			return service.pow(num1, num2);
		default:
			return null;
		}
	}
//use %2b for "+" and %5E for "^"
}
